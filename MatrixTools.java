/**********
 * MatrixTools.java
 * 
 * This class provides methods for matrix manipulation and logistic regression.
 * Methods in this class are static.
 * 
 **********/

package com.ibm.baya;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Set;

import Jama.Matrix;

public class MatrixTools {
	
	public static void printMatrixToFile(Matrix A, String outFile){
		try {
			PrintWriter out = new PrintWriter(new FileWriter(outFile, false));
			A.print(out, 6, 2);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Matrix concatRight(Matrix ... matrices) throws Exception{
		// compute final matrix dimensions;
		int m = matrices[0].getRowDimension(); // number of rows;
		int n = 0;
		for(Matrix mx : matrices){
			if(mx.getRowDimension() != m){
				//System.out.printf("expected height %d, got height %d\n",
				//		m, mx.getRowDimension());
				throw new Exception("matrix heights do not match");
			}
			n += mx.getColumnDimension(); // number of columns;
		}
		Matrix ret = new Matrix(m, n); // num cols, num rows;
		int currN = 0;
		for(Matrix mx : matrices){
			ret.setMatrix(0, m-1, currN, currN + mx.getColumnDimension() - 1, mx);
			currN += mx.getColumnDimension();
		}
		return ret;
	}
	
	public static Matrix concatDown(Matrix ... matrices) throws Exception{
		// compute final matrix dimensions;
		int m = 0;
		int n = matrices[0].getColumnDimension(); // number of columns;
		for(Matrix mx : matrices){
			if(mx.getColumnDimension() != n){
				throw new Exception("matrix widths do not match");
			}
			m += mx.getRowDimension(); // number of rows;
		}
		Matrix ret = new Matrix(m,n);
		int currM = 0;
		for(Matrix mx : matrices){
			ret.setMatrix(currM, currM + mx.getRowDimension()-1, 0, n-1, mx);
			currM += mx.getRowDimension();
		}
		return ret;
	}

	// assumes that ArrayList tokenVecs contains only 1xn row vectors;
	public static Matrix concatRowsDown(ArrayList<Matrix> tokenVecs) throws Exception {
		int m = tokenVecs.size();
		int n = tokenVecs.get(0).getColumnDimension(); // num columns;
		Matrix ret = new Matrix(m, n);
		for(int i = 0; i < tokenVecs.size(); i++){
			if(tokenVecs.get(i).getColumnDimension() != n){
				throw new Exception("matrix widths do not match");
			}
			if(tokenVecs.get(i).getRowDimension() != 1){
				throw new Exception("row vectors must be 1xn");
			}
			ret.setMatrix(i, i, 0, n-1, tokenVecs.get(i));
		}
		return ret;
	}

	// Element-wise logistic function;
	// For each element a_ij in matrix A, compute logistic(a_ij);
	public static Matrix eltWiseLogistic(Matrix A){
		int m = A.getRowDimension();
		int n = A.getColumnDimension();
		Matrix B = new Matrix(m,n);
		for(int i = 0; i < m; i++){
			for(int j = 0; j < n; j++){
				B.set(i, j, 1.0/(1.0+Math.pow(Math.E, -A.get(i, j))));
			}
		}
		return B;
	}
	
	// Element-wise natural log;
	// For each element a_ij in matrix A, compute log_e(a_ij);
	public static Matrix eltWiseNaturalLog(Matrix A){
		int m = A.getRowDimension();
		int n = A.getColumnDimension();
		Matrix B = new Matrix(m,n);
		for(int i = 0; i < m; i++){
			for(int j = 0; j < n; j++){
				B.set(i, j, Math.log(A.get(i, j))); // Math.log is natural log;
			}
		}
		return B;
	}
	
	// CAUTION: Could return NaN.
	// Checking validity of returned value is responsibility of caller.
	public static double logisticRegressionCost(Matrix X, Matrix y, Matrix theta, double lambda) {
		int m = X.getRowDimension(); // num rows in X; (should also be num rows in y);
		int n = X.getColumnDimension(); // num cols in X; (should also be num *rows* in theta);
		Matrix colOnes = new Matrix(m,1,1.0);
		//X.print(6, 2);
		//theta.print(6, 2);
		Matrix h = eltWiseLogistic(X.times(theta)); // h is now a column vector;
		//X.print(6, 2);
		
		// create a theta vector where theta_0 is set to 0;
		// will use this for regularization below;
		Matrix thetaZeroedEltZero = theta.copy();
		thetaZeroedEltZero.set(0, 0, 0.0);
		
		/****** debugging ******
		
		// no element in h should be greater than 1;
		// if an elt is greater than 1, we will try to take the log of
		// a negative number below;
		for(int i = 0; i < h.getRowDimension(); i++){
			if(h.get(i, 0) > 1 || h.get(i,  0) < 0){
				System.out.printf("ERROR: h has bad elt\n");
				break;
			}
		}
		
		// check steps in termB;
		Matrix tmp = colOnes.minus(y);
		for(int i = 0; i < tmp.getRowDimension(); i++){
			if(Double.isNaN(tmp.get(i, 0))){
				//throw new Exception("err"); 
				System.out.printf("ERR\n");
			}
		}
		
		Matrix tmp2 = eltWiseNaturalLog(colOnes.minus(h));
		for(int i = 0; i < tmp2.getRowDimension(); i++){
			if(Double.isNaN(tmp2.get(i, 0))){
				//throw new Exception("err"); 
				System.out.printf("ERR\n");
			}
		}
		
		for(int i = 0; i < tmp.getRowDimension(); i++){
			if(Double.isNaN(tmp.get(i, 0) * tmp2.get(i, 0))){
				System.out.printf("NaN Err: %.012f %.012f\n", tmp.get(i,0), tmp2.get(i, 0));
				break;
			}
		}
		
		tmp = colOnes.minus(y).transpose()
				.times(eltWiseNaturalLog(colOnes.minus(h)));
		if(tmp.getColumnDimension() != 1 || tmp.getRowDimension() != 1){
			System.out.printf("ERR\n");
		}
		if(Double.isNaN(tmp.get(0, 0))){
			System.out.printf("ERR\n");
		}
		
		****** end debugging ******/
		
		double termA = y.times(-1.0).transpose().times(eltWiseNaturalLog(h)).get(0,0);
		double termB = colOnes.minus(y).transpose()
				.times(eltWiseNaturalLog(colOnes.minus(h))).get(0,0);
		double termRegzn = thetaZeroedEltZero.transpose().times(thetaZeroedEltZero).get(0, 0);
		double ret = (termA - termB)/m + (termRegzn*lambda)/(2*m);
		
		/****** debugging ******
		if(Double.isNaN(ret)){
			System.out.printf("logisticRegressionCost() is returning NaN\n");
			System.out.printf("termA=%.020f termB=%.020f\n", termA, termB);
		}
		****** end debugging ******/
		return ret;
	}
	
	public static Matrix logisticRegressionUpdatedTheta(Matrix X, 
			Matrix y, Matrix theta, double lambda, double alpha){
		int m = X.getRowDimension();
		Matrix h = eltWiseLogistic(X.times(theta));
		Matrix thetaZeroedEltZero = theta.copy(); // for regularization;
		thetaZeroedEltZero.set(0, 0, 0.0);
		return theta.minus(X.transpose().times(h.minus(y)).times(alpha/m))
				.minus(thetaZeroedEltZero.times((-lambda * alpha)/m));
	}

	public static ArrayList<Double> computeColMeans(Matrix X) {
		int m = X.getRowDimension();
		int n = X.getColumnDimension();
		ArrayList<Double> colMeans = new ArrayList<Double>();
		for(int j = 0; j < n; j++){
			double sum = 0;
			for(int i = 0; i < m; i++){
				sum += X.get(i, j);
			}
			if(sum == 0.0){ colMeans.add(0.0); }
			else{ colMeans.add(sum/(double)m); }
		}
		//for(int i = 0; i < colMeans.size(); i++){
		//	System.out.printf("%f ", colMeans.get(i));
		//}
		//System.out.printf("\n");
		return colMeans;
	}

	// NOTE: computes std dev as sqrt(sumSquaredDiffs/num_elts), 
	// not sqrt(sumSquaredDiffs/(num_elts - 1));
	public static ArrayList<Double> computeColStdDevs(Matrix X, ArrayList<Double> colMeans) {
		int m = X.getRowDimension();
		int n = X.getColumnDimension();
		ArrayList<Double> colStdDevs = new ArrayList<Double>();

		for(int j = 0; j < n; j++){
			double sigma = 0;
			for(int i = 0; i < m; i++){
				sigma += Math.pow(X.get(i, j) - colMeans.get(j), 2);
			}
			sigma = Math.sqrt(sigma/(double)m);
			if(colMeans.get(j) == 0){ colStdDevs.add(1.0); }
			else{ colStdDevs.add(sigma); }
		}
		//for(int i = 0; i < colStdDevs.size(); i++){
		//	System.out.printf("%f ", colStdDevs.get(i));
		//}
		//System.out.printf("\n");
		return colStdDevs;
	}
	
	// For each column of X, subtracts mean, divides by standard deviation.
	public static Matrix colWiseCenterAndScale(Matrix X,
			ArrayList<Double> colMeans, ArrayList<Double> colStdDevs){
		
		/*
		int m = X.getRowDimension();
		int n = X.getColumnDimension();
		Matrix ret = new Matrix(m,n);
		for(int j = 0; j < n; j++){
			double sum = 0;
			for(int i = 0; i < m; i++){
				sum += X.get(i, j);
			}
			if(sum == 0.0){ continue; }
			double mean = sum / m;
			double sigma = 0;
			for(int i = 0; i < m; i++){
				sigma += Math.pow(X.get(i, j) - mean, 2);
			}
			sigma = Math.sqrt(sigma/m);
			for(int i = 0; i < m; i++){
				ret.set(i, j, (X.get(i, j) - mean)/sigma);
			}
		}
		return ret;
		*/
		
		int m = X.getRowDimension();
		int n = X.getColumnDimension();
		Matrix ret = new Matrix(m,n);
		for(int j = 0; j < n; j++){
			for(int i = 0; i < m; i++){
				ret.set(i, j, (X.get(i, j) - colMeans.get(j))/colStdDevs.get(j));
				//ret.set(i, j, X.get(i, j));
			}
		}
		return ret;
	}
	
	// caller is responsible for prepending x_0 to each x vector beforehand;
	// caller is responsible for centering and scaling X beforehand;
	// caller is responsible for providing initial theta, lambda, maxIter, epsilon;
	// function is responsible for choosing initial alpha;
	// function refines alpha using exponential growth/backoff;
	public static Matrix logisticGradientDescent(Matrix X, Matrix y, 
			Matrix initialTheta, double lambda, int maxIter, double epsilon){
		boolean verbose = false;
		
		Matrix currTheta = initialTheta.copy();
		Matrix newTheta;
		double currCost = logisticRegressionCost(X, y, currTheta, lambda);
		if(verbose){System.out.printf("initial cost: %.04f\n", currCost);}
		double newCost = currCost - 2 * epsilon; // newCost must begin smaller than currCost;
		double alpha = 1;
		int phase = 0;  // phase 0 is growth; phase 1 is backoff;

		for(int i = 0; i < maxIter; i++){
			
			// compute new cost;
			newTheta = logisticRegressionUpdatedTheta(X, y, currTheta, lambda, alpha);
			newCost = logisticRegressionCost(X, y, newTheta, lambda);
			//System.out.printf("alpha: %.04f\n", alpha);
			//System.out.printf("newCost: %.04f\n", newCost);
			
			// print status;
			if(i % 100 == 0 || i < 20){
				if(verbose){
					System.out.printf("iter %d: currCost=%.04f newCost=%.04f " +
						"diff=%.012f alpha=%.012f phase=%d\n",
						i, currCost, newCost, currCost - newCost, alpha, phase);
				}
			}
			
			if(Double.isNaN(newCost)){
				// newTheta pushed an element of h too close to 0;
				// this will cause an element of log(1-h) to be negative infinity;
				// discard newTheta and try again with smaller alpha;
				if(i % 100 == 0 || i < 20){
					if(verbose){
						System.out.printf("iter %d: newCost was NaN; discarding newTheta; " +
								"setting alpha to %.012f\n", i, alpha/2);
					}
				}
				if(phase == 0){
					phase = 1;
				}
				alpha /= 2;
				//continue;
			}
			else{
				// we have a meaningful cost value;
				// update alpha, check quit condition, then update currCost and currTheta;
				
				if(phase == 0){ // growth phase;
					if(newCost > currCost){
						// cost is growing; we should try smaller alpha;
						alpha /= 2;
						phase = 1;
					}
					else{
						// it's okay to try a larger alpha;
						alpha *= 2;
					}
				}
				else if(phase == 1){ // backoff phase;
					if(newCost > currCost){
					//if(cost > prevCost){
						alpha /= 2;
					}
				} // end phase logic;
				
				// check quit condition;
				if(Math.abs(currCost - newCost) < epsilon){ 
					if(verbose){
						System.out.printf("stopping: cost diff is less than epsilon\n");
						System.out.printf("iter %d: currCost=%.04f newCost=%.04f " +
								"diff=%.012f alpha=%.012f phase=%d\n",
								i, currCost, newCost, currCost - newCost, alpha, phase);
					}
					currTheta = newTheta;
					break; 
				}
				
				currTheta = newTheta;
				currCost = newCost;
			} // end meaningful cost value;
			
			if(alpha < Double.MIN_VALUE * 16) {
				if(verbose){
					System.out.printf("stopping: alpha too small; try different initial theta;\n");
					System.out.printf("iter %d: currCost=%.04f newCost=%.04f " +
							"diff=%.012f alpha=%.012f phase=%d\n",
							i, currCost, newCost, currCost - newCost, alpha, phase);
				}
				break;
			}
		} // end maxIter loop;
		return currTheta;
	}
	
	// given a matrix with a categorical column, expand that column
	// into multiple columns of indicator variables;
	// CAUTION: if applying to multiple columns, must apply right to left?
	public static Matrix categoricalToIndicator(Matrix A, int col, ArrayList<Double> classificationLabels){
		int m = A.getRowDimension();
		int n = A.getColumnDimension();
		Matrix c = A.getMatrix(0,m-1,col,col);
		//for(int i = 0; i < classificationLabels.size(); i++){
		//	System.out.printf("%f ", classificationLabels.get(i));
		//}
		//System.out.printf("\n");
		//System.out.printf("m:%d n:%d\n",  m, n);
		//c.print(6, 2);
		
		Matrix expanded = new Matrix(m,classificationLabels.size());
		for(int i = 0; i < m; i++){
			int labelIdx = classificationLabels.indexOf(c.get(i, 0));
			if(labelIdx >= 0){
				expanded.set(i, classificationLabels.indexOf(c.get(i, 0)), 1);
			}
		}
		//expanded.print(6, 2);
		
		Matrix left = A.getMatrix(0, m-1,0,col-1);
		Matrix right = A.getMatrix(0, m-1,col+1,n-1);
		Matrix complete = null;
		try {
			complete = concatRight(left, expanded, right);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return complete;
	}
	
	public static ArrayList<String> expandColumnLabels(ArrayList<String> labels, int col, int expandTo){
		String columnLabel = labels.get(col).substring(0, 4);
		ArrayList<String> expandedLabels = new ArrayList<String>();
		int sizeLeft = col;
		int sizeMid = expandTo - labels.size() + 1;
		int sizeRight = labels.size() - col - 1;
		int replaceWith = expandTo - labels.size();
		//System.out.printf("%d %d %d\n", sizeLeft, sizeMid, sizeRight);
		
		for(int i = 0; i < sizeLeft; i++){
			expandedLabels.add(labels.get(i));
			//System.out.printf("%s ", labels.get(i));
		}
		for(int i = 0; i < sizeMid; i++){
			StringBuffer buffer = new StringBuffer();
			Formatter formatter = new Formatter(buffer);
			formatter.format("%s%04d", columnLabel, i);
			expandedLabels.add("" + formatter);
			//System.out.printf("%s ", "" + formatter);
		}
		for(int i = sizeLeft + 1; i < labels.size(); i++){
			expandedLabels.add(labels.get(i));
			//System.out.printf("%s ", labels.get(i));
		}
		//System.out.printf("\n");
		
		return expandedLabels;
	}

	public static ArrayList<Double> listClassifications(Matrix A, int col) {
		int m = A.getRowDimension();
		int n = A.getColumnDimension();
		Matrix c = A.getMatrix(0,m-1,col,col);
		
		// build a set of categories;
		Set<Double> categories = new HashSet<Double>();
		for(int i = 0; i < m; i++){
			categories.add(c.get(i, 0));
		}
		
		ArrayList<Double> categoryList = new ArrayList<Double>();
		for(Double f : categories){
			categoryList.add(f);
		}
		Collections.sort(categoryList);
		
		return categoryList;
	}
	
	public static void test(){
		
		// basic JAMA matrix library functionality;
		
		double[][] array = {{1,2,3},{4,5,6},{7,8,10}};
		Matrix A = new Matrix(array);
		Matrix b = Matrix.random(3,1);
		Matrix x = A.solve(b);
		Matrix Residual = A.times(x).minus(b);
		double rnorm = Residual.normInf();
		//System.out.printf("%f\n", rnorm);
		
		// test concatenation functions from MatrixTools;
		
		//A.print(6, 2);
		array[0][0] = 35; // modifying array modifies A;
		//A.print(6, 2);
		
		double[][] arrayB = {{101,102,103},{104,105,106},{107,108,110}};
		Matrix B = new Matrix(arrayB);
		//B.print(6,  2);
		//b.print(6, 2);

		Matrix C = null;
		try {
			C = MatrixTools.concatRight(A, b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//C.print(6,  2);
		
		Matrix D = null;
		try {
			D = MatrixTools.concatRight(B, b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//D.print(6,  2);
		
		Matrix E = null;
		try {
			E = MatrixTools.concatDown(C, D);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//E.print(6,  2);
		
		// check time to do mat-mats, mat-vecs;
		
		long startTime, stopTime, duration;
		int m = 2000;
		//A = Matrix.random(m,m);
		//B = Matrix.random(m,m);
		startTime = System.nanoTime();
		//C = A.times(B);
		stopTime = System.nanoTime();
		duration = (stopTime - startTime);
		//System.out.printf("time to do mat-mat: %.03f msecs\n", ((double)duration)/1000000);
		
		// test logistic regression functions;
		
		double arr[][] = {{1},{2},{3},{7}};
		Matrix p = new Matrix(arr);
		//MatrixTools.eltWiseNaturalLog(p).print(6, 2);
		
		double arrX1[][] = {
				{1, 2, 3},
				{2, 3, 4},
				{3, 4, 5},
				{4, 5, 6},
				{5, 6, 7},
				{6, 7, 8},
				{7, 8, 9}
		};
		double arrX2[][] = {
				{3, 5, 8},
				{2, 4, 2},
				{8, 5, 8},
				{4, 4, 3},
				{1, 8, 9},
				{-3, 7, 7},
				{2, 2, 2},
		};

		LCG lcg = new LCG(20);
		//LCG lcg = new LCG(80);
		//int randMatrixNumRows = 100;
		int randMatrixNumRows = 1000;
		//int randMatrixM = 10000;
		double arrX3[][] = new double[randMatrixNumRows][3];
		for(int i = 0; i < arrX3.length; i++){
			for(int j = 0; j < arrX3[0].length; j++){
				//arrX3[i][j] = (lcg.getRand() % 10) * (j*100 + 1) + 4;
				arrX3[i][j] = (lcg.getRand() % 10);
			}
		}

		double arrY1[][] = {{0}, {0}, {0}, {0}, {1}, {0}, {1}};
		double arrY3[][] = new double[arrX3.length][1];
		for(int i = 0; i < arrY3.length; i++){
			//arrY[i][0] = 400 + -300 * arrX3[i][0] + 20 * arrX3[i][1] + 1000 * arrX3[i][2];
			//arrY3[i][0] = 20 * arrX3[i][2];
			arrY3[i][0] =  -10 * arrX3[i][0] + 30 * arrX3[i][2];
		}
		// convert y values to 0's, 1's;
		double meanY = 0;
		for(int i = 0; i < arrY3.length; i++){
			meanY += arrY3[i][0];
		}
		meanY /= arrY3.length;
		for(int i = 0; i < arrY3.length; i++){
			if(arrY3[i][0] / meanY > 0.5){
				arrY3[i][0] = 1;
			}
			else{
				arrY3[i][0] = 0;
			}
		}
		
		double arrInitialTheta[][] = {{.1}, {.1}, {.1}, {.1}};
		//double arrInitialTheta[][] = {{.3}, {.2}, {.3}, {.2}};
		//double arrInitialTheta[][] = {{10}, {20}, {-10}, {40}}; // causes cost to be NaN;
		//double arrInitialTheta[][] = {{4}, {-2}, {3}, {-1}};
		//Matrix X = new Matrix(arrX1);
		//Matrix X = new Matrix(arrX2);
		//Matrix X = Matrix.random(7, 3);
		Matrix X = new Matrix(arrX3);
		// center and scale;
		//X.print(6, 2);
		//X = colWiseCenterAndScale(X);
		ArrayList<Double> colMeans = computeColMeans(X);
		ArrayList<Double> colStdDevs = computeColStdDevs(X, colMeans);
		X = colWiseCenterAndScale(X, colMeans, colStdDevs);
		//X.print(6, 2);
		//MatrixTools.printMatrixToFile(X, "/Users/checedaaronrodgers/Desktop/X.txt");
		
		
		//Matrix y = new Matrix(arrY1);
		Matrix y = new Matrix(arrY3);
		//y.print(6, 2);
		//MatrixTools.printMatrixToFile(y, "/Users/checedaaronrodgers/Desktop/y.txt");
		Matrix initialTheta = new Matrix(arrInitialTheta);
		
		
		// add x_0 variable to every x vector;
		Matrix x_0s = new Matrix(X.getRowDimension(), 1, 1);
		Matrix XWIntercept = null;
		try { XWIntercept = concatRight(x_0s, X); } catch (Exception e) { e.printStackTrace(); }
		//X.print(6, 2);
		
		double epsilon = .0000001;
		//double epsilon = .0001;
		int maxIter = 8000;
		//int maxIter = 8;
		//double lambda = 0.0;
		double lambda = 4.0; // larger lambda means smaller theta parameters;
		//Matrix theta = logisticGradientDescent(XWIntercept, y, initialTheta, lambda, maxIter, epsilon);
		//theta.print(6, 2);
		
		if(Double.isNaN(Double.NEGATIVE_INFINITY)){
			System.out.printf("negative infinity is not a number\n");
		}
		else{
			System.out.printf("negative infinity is a number\n");
		}
		
		// train on 900, test on 100
		/*
		Matrix XTrain = XWIntercept.getMatrix(0, 899,0,3);
		Matrix yTrain = y.getMatrix(0,899,0,0);
		System.out.printf("%d %d\n", XTrain.getRowDimension(), XTrain.getColumnDimension());
		System.out.printf("%d %d\n", yTrain.getRowDimension(), yTrain.getColumnDimension());
		System.out.printf("%d %d\n", initialTheta.getRowDimension(), initialTheta.getColumnDimension());
		Matrix thetaTrain = logisticGradientDescent(XTrain, yTrain, 
				initialTheta, lambda, maxIter, epsilon);
		Matrix XTest = XWIntercept.getMatrix(900, 999, 0, 3);
		Matrix yTest = y.getMatrix(900, 999, 0, 0);
		Matrix yDotProd = XTest.times(thetaTrain);
		Matrix yProb = MatrixTools.eltWiseLogistic(yDotProd);
		Matrix yPredicted = yDotProd.copy();
		for(int i = 0; i < yPredicted.getRowDimension(); i++){
			if(yPredicted.get(i, 0) > 0.5){ yPredicted.set(i, 0, 1.0); }
			else{ yPredicted.set(i, 0, 0.0); }
		}
		Matrix yWrong = yDotProd.copy();
		for(int i = 0; i < yPredicted.getRowDimension(); i++){
			if(yTest.get(i, 0) != yPredicted.get(i, 0)){ yWrong.set(i, 0, 1.0); }
			else{ yWrong.set(i, 0, 0.0); }
		}
		
		try {
			MatrixTools.concatRight(yTest, yDotProd, yProb, yPredicted, yWrong).print(6, 2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		double[][] arrF = {
				{9,2,9},
				{9,3,9},
				{9,4,9},
				{9,2,9},
				{9,3,9},
		};
		ArrayList<String> colLabels = new ArrayList<String>();
		colLabels.add("Aaaaaa");
		colLabels.add("Bbbbbb");
		colLabels.add("Cccccc");
		Matrix F = new Matrix(arrF);
		F.print(6, 2);
		ArrayList<Double> classifications = listClassifications(F, 1);
		Matrix FInd = categoricalToIndicator(F, 1, classifications);
		FInd.print(6, 2);
		colLabels = expandColumnLabels(colLabels, 1, FInd.getColumnDimension());
	}
}
